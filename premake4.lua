solution "OpenGL_training"
	configurations ""
	buildoptions({"-std=c++11","-Wextra", "-O2", "-s", "-DFREEGLUT_STATIC", "-DGLEW_STATIC"})

	project "OpenGL_training"
		kind "WindowedApp"
		language "C++"
		includedirs { "src" }
		files {
			"src/*.cpp", "src/*.hpp",
			"src/**/*.cpp", "src/**/*.hpp"
		}

		flags({"Symbols", "ExtraWarnings"})

		linkoptions({"-std=c++11", "-lsfml-graphics", "-lsfml-window", "-lsfml-system",
			"-lpthread", "-lm", "-lGL", "-lSOIL", "-L/usr/X11R6/lib", "-lGLU", "-lglut",
			"-lGLEW", "-lglfw", "-lassimp"})
