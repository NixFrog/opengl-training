# README #

This is a quick school project for 2D cloth simulation, using a small boilerplate code for OpenGL.

## Compiling
This project requires glfw3, glew, glut, and glm to compile. It uses premake for build configuration; in order to generate and use a simple makefile just run:

```
premake4 gmake && make
```

## Running
The executable runs as is and takes no arguments. This project was tested under Fedora 25, and is not compatible with Wayland, so please ensure you are running X.