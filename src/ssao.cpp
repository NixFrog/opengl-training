// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "../include/physics.hpp"
#include "../include/point-mass.hpp"
#include "../include/link.hpp"
#include "../include/camera.hpp"
#include "../include/shader.hpp"

// GLM Mathemtics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other Libs
#include <SOIL/SOIL.h>
#include <iostream>

#include <random> // necessary for generation of random floats (for sample kernel and noise texture)

// Properties
const GLuint SCR_WIDTH = 1280, SCR_HEIGHT = 720;

// Function prototypes
void initGLFW();
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void init();
void draw();
void createCurtain();

GLFWwindow* window;


GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLuint gBuffer, gPosition, gNormal, gAlbedo;
GLuint ssaoFBO, ssaoBlurFBO, ssaoColorBuffer, ssaoColorBufferBlur;
std::vector<glm::vec3> ssaoKernel;
std::default_random_engine generator;
std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0);
std::vector<glm::vec3> ssaoNoise;
GLuint noiseTexture;

GLuint draw_mode = 1;

Physics physics;
std::vector<std::shared_ptr<PointMass>> pointMasses;
int gravity = 980;
int curtainHeight = 40;
int curtainWidth = 60;
int restingDistances = 15;
int stiffnesses = 1;
int yStart = 25;

Shader *curtainShader;

GLfloat lerp(GLfloat a, GLfloat b, GLfloat f)
{
    return a + f * (b - a);
}

void init()
{
	curtainShader = new Shader("shaders/curtain.vs", "shaders/curtain.fs");
	createCurtain();
}

void createCurtain()
{
	int midWidth = SCR_WIDTH/2 - (curtainWidth * restingDistances) / 2;

	for (int y = 0; y <= curtainHeight; y++){
		for (int x = 0; x <= curtainWidth; x++){
			std::shared_ptr<PointMass> pointMass = std::make_shared<PointMass>(
					midWidth + x * restingDistances
					, y * restingDistances + yStart
			);

			if (x != 0){
				pointMass->attachTo(
						pointMasses.at(pointMasses.size()-1)
						, restingDistances
						, stiffnesses
						, 30
						, true
				);
			}

			if (y != 0){
				//std::cout<<x<<" y "<<y<< " to "<<pointMasses.at((y-1)*curtainWidth+x).x<<std::endl;
				auto& p = pointMasses.at((y-1)*curtainWidth + x);
				pointMass->attachTo(p, restingDistances, stiffnesses, 30, true);
			}

			if (y == 0){
				pointMass->pinTo(pointMass->x, pointMass->y);
			}

			pointMasses.push_back(pointMass);
		}
	}
}

void drawPoints(const std::vector<GLfloat>& points)
{
	GLuint vao, vbo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(GLfloat)
				, &points[0] , GL_STATIC_DRAW);
		glEnable(GL_PROGRAM_POINT_SIZE);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	glBindVertexArray(vao);
	curtainShader->Use();
	glDrawArrays(GL_POINTS, 0, points.size() / 2);
}

void drawLines(const std::vector<GLfloat>& lines)
{
	GLuint vao, vbo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, lines.size() * sizeof(GLfloat)
				, &lines[0] , GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	glBindVertexArray(vao);
	curtainShader->Use();
	glDrawArrays(GL_LINES, 0, lines.size() / 2);
}

void draw()
{
	std::vector<GLfloat> points;


	for(auto pointMass : pointMasses){
		//if(pointMass.links.size() > 0) {
			//for(auto link : pointMass.links){
			//}
		//}
		//else{
			float x_scr = (pointMass->x - SCR_WIDTH/2) / SCR_WIDTH;
			float y_scr = (SCR_HEIGHT/2 - pointMass->y) / SCR_HEIGHT;
			points.push_back(x_scr);
			points.push_back(y_scr);
		//}
	}

	drawPoints(points);
}

int main()
{
	initGLFW();
	init();

	while (!glfwWindowShouldClose(window))
	{
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);

		glfwPollEvents();

		physics.update(pointMasses);
		draw();

		// Swap the buffers
		glfwSwapBuffers(window);
	}

	glfwTerminate();
	return 0;
}

void initGLFW()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", nullptr, nullptr); // Windowed
	glfwMakeContextCurrent(window);

	// Initialize GLEW to setup the OpenGL Function pointers
	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();

	// Define the viewport dimensions
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

	// Setup some OpenGL options
	glEnable(GL_DEPTH_TEST);

}

