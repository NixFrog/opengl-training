#pragma once

#include "point-mass.hpp"
#include "link.hpp"
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Physics
{
	public:
		float previousTime;
		float currentTime;

		int fixedDeltaTime;
		float fixedDeltaTimeSeconds;

		int leftOverDeltaTime;

		int constraintAccuracy;

		Physics() {
			fixedDeltaTime = 16;
			fixedDeltaTimeSeconds = (float)fixedDeltaTime / 1000.0;
			leftOverDeltaTime = 0;
			constraintAccuracy = 3;
		}

		// Update physics
		void update(std::vector<std::shared_ptr<PointMass>>& pointmasses) {
			currentTime = glfwGetTime() * 1000;
			long deltaTimeMS = currentTime - previousTime;
			previousTime = currentTime;

			int timeStepAmt = (int)((float)(deltaTimeMS + leftOverDeltaTime) / (float)fixedDeltaTime);
			timeStepAmt = std::min(timeStepAmt, 5);
			leftOverDeltaTime = (int)deltaTimeMS - (timeStepAmt * fixedDeltaTime);

			for(int iteration = 1; iteration <= timeStepAmt; iteration++){
				for(int x = 0; x < constraintAccuracy; x++){
					for(auto pointmass : pointmasses){
						for(auto& link : pointmass->links){
							link.solve();
						}
						pointmass->solveConstraints();
					}
				}

				for(auto& pointmass : pointmasses){
					pointmass->updatePhysics(fixedDeltaTimeSeconds);
				}
			}
		}
};
