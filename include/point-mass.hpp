#pragma once

#include <vector>
#include <algorithm>
#include <memory>

class Link;

class PointMass
	: public std::enable_shared_from_this<PointMass>
{
	public:
		float lastX, lastY; // for calculating position change (velocity)
		float x,y;
		float accX, accY;

		float mass = 1;
		float damping = 20;

		std::vector<Link> links;

		bool pinned = false;
		float pinX, pinY;
		int gravity = 980;

		// PointMass constructor
		PointMass(float xPos, float yPos) {
			x = xPos;
			y = yPos;

			lastX = x;
			lastY = y;

			accX = 0;
			accY = 0;
		}

		// The update function is used to update the physics of the PointMass.
		// motion is applied, and links are drawn here
		void updatePhysics(float timeStep) { // timeStep should be in elapsed seconds (deltaTime)
			if(!pinned){
				applyForce(0, mass * gravity);
			}

			float velX = x - lastX;
			float velY = y - lastY;

			// dampen velocity
			velX *= 0.99;
			velY *= 0.99;

			float timeStepSq = timeStep * timeStep;

			// calculate the next position using Verlet Integration
			float nextX = x + velX + 0.5 * accX * timeStepSq;
			float nextY = y + velY + 0.5 * accY * timeStepSq;

			// reset variables
			lastX = x;
			lastY = y;

			x = nextX;
			y = nextY;

			accX = 0;
			accY = 0;
		}

		void solveConstraints() {
			if(pinned){
				x = pinX;
				y = pinY;
			}
			else{
				solveLinkConstraints();
				solveBoundaryConstraints();
			}
		}

		void solveLinkConstraints();

		void solveBoundaryConstraints(){
			int height = 700;
			int width = 1270;

			if(y < 1){y = 2 * (1) - y; }
			if(y > height-1){y = 2 * (height - 1) - y;}

			if(x > width-1){x = 2 * (width - 1) - x;}
			if(x < 1){	x = 2 * (1) - x;}
		}

		void attachTo(const std::shared_ptr<PointMass>& P, float restingDist, float stiff, float tearSensitivity, bool drawLink);

		void applyForce(float fX, float fY) {
			// acceleration = (1/mass) * force
			// or
			// acceleration = force / mass
			accX += fX/mass;
			accY += fY/mass;
		}

		void pinTo (float pX, float pY)
		{
			pinned = true;
			pinX = pX;
			pinY = pY;
		}
};
