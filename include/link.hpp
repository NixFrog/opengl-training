#pragma once

#include "point-mass.hpp"
#include <cmath>
#include <memory>

class Link
{
	public:
		std::shared_ptr<PointMass> p1;
		std::shared_ptr<PointMass> p2;

		float restingDistance;
		float stiffness;
		float tearSensitivity;

		bool drawThis = true;

		Link(const std::shared_ptr<PointMass>& which1, const std::shared_ptr<PointMass>& which2
				, float restingDist, float stiff, float tearSensitivity, bool drawMe)
			: p1(move(which1))
			, p2(move(which2))
			, restingDistance(restingDist)
			, stiffness(stiff)
			, tearSensitivity(tearSensitivity)
			, drawThis(drawMe)
		{}

		// Solve the link constraint
		void solve() {
			float diffX = p1->x - p2->x;
			float diffY = p1->y - p2->y;
			float d = sqrt(diffX * diffX + diffY * diffY);

			// find the difference, or the ratio of how far along the restingDistance the actual distance is.
			float difference = (restingDistance - d) / d;

			// if the distance is more than curtainTearSensitivity, the cloth tears
			//if (d > tearSensitivity)
				//p1.removeLink(*this);

			// Inverse the mass quantities
			float im1 = 1 / p1->mass;
			float im2 = 1 / p2->mass;
			float scalarP1 = (im1 / (im1 + im2)) * stiffness;
			float scalarP2 = stiffness - scalarP1;

			// Push/pull based on mass
			// heavier objects will be pushed/pulled less than attached light objects
			p1->x += diffX * scalarP1 * difference;
			p1->y += diffY * scalarP1 * difference;

			p2->x -= diffX * scalarP2 * difference;
			p2->y -= diffY * scalarP2 * difference;
		}

		// Draw if it's visible
		void draw() {
			//if(drawThis)
				//line(p1.x, p1.y, p2.x, p2.y);
		}
};

void PointMass::attachTo(const std::shared_ptr<PointMass>& P, float restingDist, float stiff, float tearSensitivity, bool drawLink) {
	Link lnk(shared_from_this(), P, restingDist, stiff, tearSensitivity, drawLink);
	links.push_back(lnk);
}

void PointMass::solveLinkConstraints()
{
	for(auto& link : links){
		link.solve();
	}
}
